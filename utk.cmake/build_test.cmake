#############################################################################
# Copyright 2018-2024 Utility Tool Kit Open Source Contributors             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
#     http://www.apache.org/licenses/LICENSE-2.0                            #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################


cmake_minimum_required (VERSION 3.9)

# @function utk_cmake_add_build_test_suite
#
# @brief Produces a set of tests checking if the code can be compiled or not
#
# @details Uses the provided source file for producing a bunch of `ctest
# --build-and-test` tests using the provided project template. The source file
# should be marked up with conditional compilation macros for choosing what part
# of the code is checked for whether it compiles or not. An example project
# template can be found at utk.cmake/build_test/CMakeLists.txt.in
#
#
# @param [in] i_file - the test source file. File name is used as the test suite
#                      name. Must be an absolute file name.
#
# @param [in] i_project_template - a template for generating all of test
#                                  projects. Must be an absolute file name.
function (utk_cmake_add_build_test_suite i_file i_project_template)
  ###################################
  # Check for input files existence #
  ###################################
  if (NOT EXISTS "${i_file}")
    message (SEND_ERROR "File \"${i_file}\" doesn't exist")

    return ()
  endif ()

  if (NOT EXISTS "${i_project_template}")
    message (SEND_ERROR "File \"${i_project_template}\" doesn't exist")

    return ()
  endif ()

  ##############################
  # Get all test case switches #
  ##############################
  file (READ ${i_file} _test_suite_source)

  set (
    _test_case_switch_regex
    "#(el)?if[ \t]+(UTK_CMAKE_TEST_CASE_([A-Za-z0-9_]+)(_WILL_FAIL)?)")

  string(REGEX MATCHALL "${_test_case_switch_regex}"
    _test_case_switches "${_test_suite_source}")

  set (_duplicate_check ${_test_case_switches})

  ########################
  # Check for duplicates #
  ########################
  list (REMOVE_DUPLICATES _duplicate_check)

  if ("${_test_case_switches}" AND NOT _duplicate_check EQUAL _test_case_switches)
    set (_error_message "Test case duplication:\n")

    foreach (_test_case IN LISTS _duplicate_check)
      list (FIND _test_case_switches ${_test_case} _index)

      list (REMOVE_AT _test_case_switches ${_index})

      list (FIND _test_case_switches ${_test_case} _index)

      if (NOT _index EQUAL -1)
        string (APPEND _error_message "\t${_test_case}\n")
      endif ()
    endforeach()

    message (SEND_ERROR ${_error_message})

    return ()
  endif ()

  ####################################
  # Generate projects for test cases #
  ####################################
  set (_test_source_file "${i_file}")

  get_filename_component (_test_suite "${i_file}" NAME_WE)

  foreach (_test_case_switch IN LISTS _test_case_switches)
    # Extract test case toggle macro and name
    string (REGEX MATCH "${_test_case_switch_regex}" _ "${_test_case_switch}")

    set (_enabled_test_case "${CMAKE_MATCH_2}")
    set (_test_case "${CMAKE_MATCH_3}")

    # Extract WILL_FAIL test property value
    if (_test_case MATCHES "[A-Za-z0-9_]+_WILL_FAIL")
      set (_will_fail true)

      string (REPLACE "_WILL_FAIL" "" _test_case "${_test_case}")
    else ()
      set (_will_fail false)
    endif ()

    ##############################
    # Generate test case project #
    ##############################
    set (
      _test_src_dir
      "${CMAKE_CURRENT_BINARY_DIR}/${_test_suite}/src/${_test_case}"
      )
    set (
      _test_build_dir
      "${CMAKE_CURRENT_BINARY_DIR}/${_test_suite}/build/${_test_case}"
      )

    file (MAKE_DIRECTORY "${_test_src_dir}")

    configure_file (
      "${i_project_template}"
      "${_test_src_dir}/CMakeLists.txt"
      @ONLY
      )

    set (_test_name "${_test_suite}::${_test_case}")

    # Based on https://stackoverflow.com/a/50665823/1292814
    add_test(
      NAME "${_test_name}"
      COMMAND ${CMAKE_CTEST_COMMAND}
      --build-and-test
      "${_test_src_dir}"
      "${_test_build_dir}"
      --build-generator ${CMAKE_GENERATOR}
      --test-command ${CMAKE_CTEST_COMMAND}
      )

    set_tests_properties("${_test_name}" PROPERTIES WILL_FAIL ${_will_fail})
  endforeach()
endfunction (utk_cmake_add_build_test_suite)
